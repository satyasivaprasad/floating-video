package com.magic.myvideoplayerexp

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat

class NotificationUtils {

    companion object {

        fun createChannel(context: Context) {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            if (VdyosUtils.checkBuildVersion() && notificationManager.getNotificationChannel(VdyosUtils.NOTIFICATION.FOREGROUND_CHANNEL_ID) == null) {

                // Create the NotificationChannel, but only on API 26+ because
                // the NotificationChannel class is new and not in the support library
                val importance = NotificationManager.IMPORTANCE_HIGH
                val notificationChannel = NotificationChannel(VdyosUtils.NOTIFICATION.FOREGROUND_CHANNEL_ID, VdyosUtils.NOTIFICATION.CHANNEL_NAME, importance)
                notificationChannel.enableVibration(true)
                notificationChannel.setShowBadge(true)
                notificationChannel.enableLights(true)
                notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                notificationManager.createNotificationChannel(notificationChannel)
            }
        }

        fun createNotificationBuilder(context: Context, status: String): Notification {

            val notificationBuilder: NotificationCompat.Builder = if (VdyosUtils.checkBuildVersion()) {
                NotificationCompat.Builder(context, VdyosUtils.NOTIFICATION.FOREGROUND_CHANNEL_ID)
            } else {
                NotificationCompat.Builder(context)
            }
            notificationBuilder
                    .setContentTitle(status)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setOnlyAlertOnce(true)
                    .setOngoing(true)
                    .setAutoCancel(true)
                    .build()
            return notificationBuilder.build()
        }

    }
}