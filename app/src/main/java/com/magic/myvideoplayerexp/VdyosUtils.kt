package com.magic.myvideoplayerexp

import android.app.ActivityManager
import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
object VdyosUtils {

    object NOTIFICATION {
        const val NOTIFICATION_ID_FOREGROUND_SERVICE_TRIGGER = 1212121211
        const val NOTIFICATION_ID_FOREGROUND_SERVICE_BUBBLE = 1212121212
        const val FOREGROUND_CHANNEL_ID = "com.vdyos.foreground.channel"
        const val CHANNEL_NAME = "Vdyos"
    }


    fun isSdk26AndAbove(context: Context): Boolean {
        val targetSdkVersion: Int = getTargetSdkVersion(context)
        val result =
            Build.VERSION_CODES.O <= Build.VERSION.SDK_INT && Build.VERSION_CODES.O <= targetSdkVersion
        return result
    }

    fun getTargetSdkVersion(context: Context): Int {
        return context.getApplicationInfo().targetSdkVersion
    }

    fun startServiceIntent(context: Context?, intent: Intent?) {
        if (context != null && intent != null) {
            if (isSdk26AndAbove(context)) {
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }
    }

    fun startForegroundService(service: Service?, flag: Int) {
        if (service != null) {
            val notification =
                NotificationCompat.Builder(service)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("")
                    .build()
            service.startForeground(flag, notification)

        }
    }

    fun stopForegroundService(service: Service?) {
        if (service != null) {
            service.stopForeground(true)
        }
    }

    fun isServiceRunning(
        context: Context,
        clazz: Class<*>
    ): Boolean {
        val manager =
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (clazz.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun checkBuildVersion(): Boolean {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
    }
}