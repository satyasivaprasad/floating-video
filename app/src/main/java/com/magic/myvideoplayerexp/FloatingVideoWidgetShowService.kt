package com.magic.myvideoplayerexp

import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.View.OnTouchListener
import android.widget.ImageButton
import android.widget.RelativeLayout
import com.devbrackets.android.exomedia.ui.widget.VideoView
import com.magic.myvideoplayerexp.VdyosUtils.isSdk26AndAbove

class FloatingVideoWidgetShowService : Service() {
    var windowManager: WindowManager? = null
    var floatingWindow: View? = null
    var floatingView: View? = null
    var playerWrapper: View? = null
    var overlayView: View? = null
    var videoView: VideoView? = null
    var increaseSize: ImageButton? = null
    var decreaseSize: ImageButton? = null
    var playVideo: ImageButton? = null
    var pauseVideo: ImageButton? = null
    var params: WindowManager.LayoutParams? = null
    private val timeoutHandler = Handler()
    private var gestureDetector: GestureDetector? = null
    private var videoWidth = 250 // Default width of floating video player
    private var videoHeight = 180 // Default Height of floating video player
    private var shouldClick = false
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent != null && intent.action != null) {
            when (intent.action) {
                "ACTION_CLOSE_WIDGET" -> {
                    val seek = videoView!!.currentPosition
                    videoView!!.keepScreenOn = false
                    stopSelf()
                    onDestroy()
                }
                "ACTION_PLAY" -> {
                    onResume(floatingWindow)
                }
                "ACTION_PAUSE" -> {
                    onPause(floatingWindow)
                }
                "ACTION_PREV" -> {
                }
                "ACTION_NEXT" -> {
                    onNext(floatingWindow)
                }
                "ACTION_SET_VIDEO" -> {
                    Log.d("VideoPlayer", "VideoPlayer ACTION_SET_VIDEO")
                    val myUri =
                        Uri.parse("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4")
                    videoView!!.setVideoURI(myUri)
                    videoView!!.start()
                    videoView!!.keepScreenOn = true
                }
            }
        }
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        gestureDetector = GestureDetector(this, SingleTapConfirm())
        floatingWindow = LayoutInflater.from(this).inflate(R.layout.floating_widget_layout, null)
        Log.d("VideoPlayer", "VideoPlayer inside oncreate")
        // Define the layout flag according to android version.
        val LAYOUT_FLAG: Int
        LAYOUT_FLAG = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            WindowManager.LayoutParams.TYPE_PHONE
        }
        // Setting layout params for floating video
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT, LAYOUT_FLAG,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT
        )
        windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        assert(windowManager != null)
        windowManager!!.addView(floatingWindow, params)
        // Define all the video view and its components
        floatingView = floatingWindow!!.findViewById(R.id.Layout_Expended)
        playerWrapper = floatingWindow!!.findViewById(R.id.view_wrapper)
        overlayView = floatingWindow!!.findViewById(R.id.overlay_view)
        videoView =
            floatingWindow!!.findViewById<View>(R.id.videoView) as VideoView
        increaseSize =
            floatingWindow!!.findViewById<View>(R.id.increase_size) as ImageButton
        decreaseSize =
            floatingWindow!!.findViewById<View>(R.id.decrease_size) as ImageButton
        playVideo =
            floatingWindow!!.findViewById<View>(R.id.app_video_play) as ImageButton
        pauseVideo =
            floatingWindow!!.findViewById<View>(R.id.app_video_pause) as ImageButton
        floatingView!!.setOnClickListener(View.OnClickListener {
            Log.d(
                "VideoPlayer",
                "VideoPlayer click"
            )
        })
        // Setting the on error Listener
        videoView!!.setOnErrorListener {
            val seek = videoView!!.currentPosition
            false
        }
        // Changes the video size when new video is loaded
        videoView!!.setOnVideoSizedChangedListener { intrinsicWidth, intrinsicHeight, pixelWidthHeightRatio ->
            val scale =
                applicationContext.resources.displayMetrics.density
            videoWidth = intrinsicWidth
            videoHeight = intrinsicHeight
            val relativeLayout =
                floatingWindow!!.findViewById<View>(R.id.view_wrapper) as RelativeLayout
            val aspectRatio =
                videoWidth.toDouble() / videoHeight.toDouble()
            if (videoHeight > videoWidth) {
                val height = (200 * scale + 0.5f).toInt()
                val width = height * aspectRatio
                relativeLayout.layoutParams.width = width.toInt()
                relativeLayout.layoutParams.height = height
            } else {
                val width = (250 * scale + 0.5f).toInt()
                val height = width / aspectRatio
                relativeLayout.layoutParams.width = width
                relativeLayout.layoutParams.height = height.toInt()
            }
        }
        floatingWindow!!.findViewById<View>(R.id.app_video_crop)
            .setOnClickListener {
                val seek = videoView!!.currentPosition
                videoView!!.keepScreenOn = false
                stopSelf()
                onDestroy()
            }
        floatingWindow!!.findViewById<View>(R.id.Layout_Expended)
            .setOnTouchListener(object : OnTouchListener {
                var X_Axis = 0
                var Y_Axis = 0
                var TouchX = 0f
                var TouchY = 0f
                override fun onTouch(
                    v: View,
                    event: MotionEvent
                ): Boolean {
                    if (gestureDetector!!.onTouchEvent(event)) {
                        floatingView!!.performClick()
                        if (overlayView!!.getVisibility() == View.VISIBLE) {
                            overlayView!!.setVisibility(View.GONE)
                            timeoutHandler.removeCallbacksAndMessages(null)
                        } else {
                            overlayView!!.setVisibility(View.VISIBLE)
                            timeoutHandler.postDelayed(
                                { overlayView!!.setVisibility(View.GONE) },
                                5000
                            )
                        }
                    } else {
                        val touches = event.pointerCount
                        if (touches > 1) {
                        }
                        when (event.action) {
                            MotionEvent.ACTION_DOWN -> {
                                shouldClick = true
                                X_Axis = params!!.x
                                Y_Axis = params!!.y
                                TouchX = event.rawX
                                TouchY = event.rawY
                                return true
                            }
                            MotionEvent.ACTION_UP -> {
                                floatingView!!.setVisibility(View.VISIBLE)
                                if (shouldClick) floatingView!!.performClick()
                                return true
                            }
                            MotionEvent.ACTION_MOVE -> {
                                shouldClick = false
                                params!!.x = X_Axis + (event.rawX - TouchX).toInt()
                                params!!.y = Y_Axis + (event.rawY - TouchY).toInt()
                                windowManager!!.updateViewLayout(floatingWindow, params)
                                return true
                            }
                        }
                    }
                    return false
                }
            })
        if (isSdk26AndAbove(this)) {
            startForeground(VdyosUtils.NOTIFICATION.NOTIFICATION_ID_FOREGROUND_SERVICE_TRIGGER,
                getNotificationToStartVideoService(resources.getString(R.string.app_name), this))
        }
    }

    fun increaseWindowSize(view: View?) {
        val scale = applicationContext.resources.displayMetrics.density
        val relativeLayout =
            floatingWindow!!.findViewById<View>(R.id.view_wrapper) as RelativeLayout
        val display = windowManager!!.defaultDisplay
        val size = Point()
        display.getSize(size)
        val densityX = size.x // default height width of screen
        val aspectRatio = videoWidth.toDouble() / videoHeight.toDouble()
        if (videoHeight > videoWidth) {
            val height = (400 * scale + 0.5f).toInt()
            val width = height * aspectRatio
            relativeLayout.layoutParams.width = width.toInt()
            relativeLayout.layoutParams.height = height
        } else {
            val height = densityX / aspectRatio
            relativeLayout.layoutParams.width = densityX
            relativeLayout.layoutParams.height = height.toInt()
        }
        increaseSize!!.visibility = View.GONE
        decreaseSize!!.visibility = View.VISIBLE
    }

    fun returnToApp(view: View?) {
        val seek = videoView!!.currentPosition
        val intent =
            packageManager.getLaunchIntentForPackage(applicationContext.packageName)
        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        videoView!!.keepScreenOn = false
        stopSelf()
        onDestroy()
    }

    fun decreaseWindowSize(view: View?) {
        val scale = applicationContext.resources.displayMetrics.density
        val relativeLayout =
            floatingWindow!!.findViewById<View>(R.id.view_wrapper) as RelativeLayout
        val aspectRatio = videoWidth.toDouble() / videoHeight.toDouble()
        if (videoHeight > videoWidth) {
            val height = (200 * scale + 0.5f).toInt()
            val width = height * aspectRatio
            relativeLayout.layoutParams.width = width.toInt()
            relativeLayout.layoutParams.height = height
        } else {
            val width = (250 * scale + 0.5f).toInt()
            val height = width / aspectRatio
            relativeLayout.layoutParams.width = width
            relativeLayout.layoutParams.height = height.toInt()
        }
        increaseSize!!.visibility = View.VISIBLE
        decreaseSize!!.visibility = View.GONE
    }

    fun onPause(view: View?) {
        val seek = videoView!!.currentPosition
        playVideo!!.visibility = ImageButton.VISIBLE
        pauseVideo!!.visibility = ImageButton.GONE
        videoView!!.pause()
    }

    fun onResume(view: View?) {
        val seek = videoView!!.currentPosition
        playVideo!!.visibility = ImageButton.GONE
        pauseVideo!!.visibility = ImageButton.VISIBLE
        videoView!!.start()
    }

    fun onNext(view: View?) {}
    override fun onDestroy() {
        super.onDestroy()
        if (floatingWindow != null) windowManager!!.removeView(floatingWindow)
    }

    private inner class SingleTapConfirm : SimpleOnGestureListener() {
        override fun onSingleTapUp(event: MotionEvent): Boolean {
            return true
        }
    }

    companion object {
        private const val index = 0 // Index of playing video in videoPlaylist
    }

    fun getNotificationToStartVideoService(status: String, context: Context): Notification {
        NotificationUtils.createChannel(context)
        return NotificationUtils.createNotificationBuilder(context, status)

    }

}